import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        // Initialize the line of the test.txt file
        String line1 = "";
        String line2 = "";
        String line3 = "";

        // Enter the path of the file here
        File file = new File("src/test.txt");

        // Initialize the Scanner
        Scanner sc = new Scanner(file);
        while(sc.hasNextLine()) {
                line1 = sc.nextLine();
                line2 = sc.nextLine();
                line3 = sc.nextLine();
        }

        // Parse the lines of the file
        String[] pos = line1.split(" ");
        String[] init = line2.split(" ");
        String instructions = line3;

        // Initialize the grid
        Grid grid = new Grid(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]));

        // Initialize the position of the vacuum
        Vacuum vacuum = new Vacuum(Integer.parseInt(init[0]), Integer.parseInt(init[1]),  init[2].charAt(0), grid);

        // For each instructions, move the vacuum as required
        for(char c : instructions.toCharArray()){
            vacuum.changeVacuumPosition(c);
        }

        // Display the result in the console
        System.out.println(vacuum.getX() + " " + vacuum.getY() + " " + vacuum.getOrientation());
    }
}
