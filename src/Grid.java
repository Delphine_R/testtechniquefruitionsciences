public class Grid {
    private int maxX;
    private int maxY;

    public Grid(int x, int y) {
        this.maxX = x;
        this.maxY = y;
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxY() {
        return maxY;
    }

}
