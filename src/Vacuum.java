public class Vacuum {
    private int x;
    private int y;
    private char orientation;
    private Grid grid;

    public Vacuum(int x, int y, char orientation, Grid grid) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
        this.grid = grid;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getOrientation() {
        return orientation;
    }

    public void changeVacuumPosition(char move) {
        switch (move) {
            case 'D':
                rotateRight();
                break;
            case 'G':
                rotateLeft();
                break;
            case 'A':
                move();
                break;
            default:
                break;
        }
    }

    private void rotateRight() {
        // Turn the vacuum to the right
        switch (this.orientation) {
            case 'N':
                this.orientation = 'E';
                break;
            case 'E':
                this.orientation = 'S';
                break;
            case 'W':
                this.orientation = 'N';
                break;
            case 'S':
                this.orientation = 'W';
                break;
            default:
                break;
        }
    }

    private void rotateLeft() {
        // Turn the vacuum to the left
        switch (this.orientation) {
            case 'N':
                this.orientation = 'W';
                break;
            case 'E':
                this.orientation = 'N';
                break;
            case 'W':
                this.orientation = 'S';
                break;
            case 'S':
                this.orientation = 'E';
                break;
            default:
                break;
        }
    }

    private void move() {
        switch (this.orientation) {
            case 'N':
                // If the vacuum is out of the grid, it don't move
                if (this.y != this.grid.getMaxY()) {
                    this.y ++;
                }
                break;
            case 'E':
                if (this.x != this.grid.getMaxX()) {
                    this.x ++;
                }
                break;
            case 'W':
                if (this.x != 0) {
                    this.x --;
                }
                break;
            case 'S':
                if (this.y != 0) {
                    this.y --;
                }
                break;
            default:
                break;
        }
    }
}
