1. Cloner le projet en utilisant la commande git clone https://gitlab.com/Delphine_R/testtechniquefruitionsciences.git

2. Importer le projet dans votre IDE.

3. Modifier le fichier "text.txt" à la racine de ce repository avec les données que vous souhaitez en entrée de ce programme.

4. Vous n'avez plus qu'à exécuter le programme ! 
